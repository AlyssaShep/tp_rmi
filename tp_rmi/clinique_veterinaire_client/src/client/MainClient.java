package client;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Scanner;

import remote.AnimalRemote;
import remote.CabinetRemote;
import remote.EspeceRemote;
import remote.ClientRemote;
import remote.DossierSuiviRemote;

public class MainClient {

public static void main(String[] args) {
		
		try {
			Scanner sc = new Scanner(System.in);
			String str = "e";
			String nomAnimal;
			String nomMaitre;
			EspeceRemote nomEspece;
			String nomRace;
			DossierSuiviRemote testCodeBase;
			char entree = 'e';
			
			ClientRemote clt = new Client();
			CabinetRemote cabinetStub = clt.getCabinetStub("rmi://" + InetAddress.getLocalHost().getHostAddress() + "/cabinet");
			ArrayList<AnimalRemote> cabinetPatients = cabinetStub.getAllPatientRemote();
			int nb_patients = cabinetPatients.size();
			cabinetStub.setNewClient(clt);
			
			System.out.println("______________Client______________");
			System.out.println("Utilisation : ");
			System.out.println("q pour quitter");
			System.out.println("r pour faire une recherche sur un animal avec son nom (exercice 2)");
			System.out.println("c pour afficher tous les animaux du cabinet");
			System.out.println("a pour ajouter un animal (exercice 3)");
			System.out.println("b test du codebase (exercice 4)");
			System.out.println("m pour tester le message d alerte pour une certaine taille de patients (exercice 5)");
			System.out.println("h affichage de l aide");
			
			while(entree != 'q' && entree != 'Q') {
				System.out.println("Votre choix ? ");
				str = sc.nextLine();
				entree = str.charAt(0);
				
				if (entree == 'q' || entree == 'Q') {
					System.exit(0);
				}
				
				if (entree == 'm' || entree == 'M') {
					
					for(int i = 0; i < 100; i++) {
						cabinetStub.setNewPatientRemote("Chat" + i, "Armee de chats", "Chat", 20, "Aucun", "Aucune");
					}
					
					cabinetPatients = cabinetStub.getAllPatientRemote();
					nb_patients = cabinetPatients.size();
					
				}
				
				if (entree == 'h' || entree == 'H') {
					System.out.println("Utilisation : ");
					System.out.println("q pour quitter");
					System.out.println("r pour faire une recherche sur un animal avec son nom (exercice 2)");
					System.out.println("c pour afficher tous les animaux du cabinet");
					System.out.println("a pour ajouter un animal (exercice 3)");
					System.out.println("b test du codebase (exercice 4)");
					System.out.println("m pour tester le message d alerte pour une certaine taille de patients (exercice 5)");
					System.out.println("h affichage de l aide");
				}
				
				if (entree == 'b' || entree == 'B') {
					clt.testCodeBase();
					testCodeBase = cabinetStub.getPatientWithNameRemote("Mona").getDossierRemote();
					System.out.println("Test CodeBase");
					System.out.println("soin : " + testCodeBase.getSoin());
				}
				
				if (entree == 'c' || entree == 'C') {
					
					System.out.println("animaux recupere sur le cabinet : ");
					
					for (int i = 0; i < nb_patients; i++) {
						
						nomAnimal = cabinetPatients.get(i).getNomAnimal();
						nomMaitre = cabinetPatients.get(i).getNomMaitre();
						nomEspece = cabinetPatients.get(i).getEspece();
						nomRace   = cabinetPatients.get(i).getRace();
						
						System.out.println("Animal : nom animal : " + nomAnimal + " nom Maitre : " + nomMaitre + " Espece : " + nomEspece.getNomEspece());
						
					}
					
				}
				
				if (entree == 'r' || entree == 'R') {
					System.out.println("Quel est le nom de votre animal ? ");
					str = sc.nextLine();
					
					AnimalRemote recup = cabinetStub.getPatientWithNameRemote(str);
					System.out.println("resultat de la recherche : ");
					if (recup != null) {
						nomAnimal = recup.getNomAnimal();
						nomMaitre = recup.getNomMaitre();
						nomEspece = recup.getEspece();
						nomRace   = recup.getRace();
					
						System.out.println("Nom animal : " + nomAnimal + " nom Maitre : " + nomMaitre + " Espece : " + nomEspece.getNomEspece());
					} else {
						System.out.println("L animal n appartient pas au cabinet");
					}
				}
				
				if (entree == 'a' || entree == 'A') {
					System.out.println("Pour ajouter un animal, il faut des informations :");
					System.out.println("Le nom de l'Animal :");
					nomAnimal = sc.nextLine();
					System.out.println("Le nom du maitre :");
					nomMaitre = sc.nextLine();
					System.out.println("L espece de l animal :");
					String nomEspeceN = sc.nextLine();
					System.out.println("Esperance de vie de l animal :");
					int esperance = sc.nextInt();
					sc.nextLine();
					
					System.out.println("Soin ? (y/n)");
					str = sc.nextLine();
					entree = str.charAt(0);
					while (entree != 'y' && entree != 'n') {
						System.out.println("Attention, il faut entrer y ou n");
						str = sc.nextLine();
						entree = str.charAt(0);
					}
					
					String typeSoin = "Aucun", pathologie = "Aucune";
					
					if(entree == 'y' || entree == 'Y') {
						System.out.println("Quel type de soin ? (Bandages, operations, etc...)");
						typeSoin = sc.nextLine();
						
					}
					
					System.out.println("L animal a-t-il une pathologie ? (y/n)");
					str = sc.nextLine();
					entree = str.charAt(0);
					while (entree != 'y' && entree != 'n') {
						System.out.println("Attention, il faut entrer y ou n");
						str = sc.nextLine();
						entree = str.charAt(0);
					}
					
					if(entree == 'y' || entree == 'Y' ) {
						System.out.println("Quelle pathologie ? (cardiaque, cancer, etc...)");
						pathologie = sc.nextLine();
					}
					
					System.out.println("Ajout de l animal au cabinet");
					cabinetStub.setNewPatientRemote(nomAnimal, nomMaitre, nomEspeceN, esperance, typeSoin, pathologie);
					cabinetPatients = cabinetStub.getAllPatientRemote();
					nb_patients = cabinetPatients.size();
					
				}
			}
			
			cabinetStub.removeClient(clt);
			
		} catch (Exception e) {
			System.err.println("client exception");
			e.printStackTrace();
		}
	}

}
