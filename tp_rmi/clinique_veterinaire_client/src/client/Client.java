package client;
import java.io.File;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;

import remote.*;

public class Client extends UnicastRemoteObject implements ClientRemote {
	
	private static final long serialVersionUID = 1L;
	private Registry registreClient;
	private CabinetRemote patients;
	
	public Client() throws RemoteException {
		File file = new File("client.policy");
		String path = file.getPath();
		System.setProperty("java.security.policy","file:" + path);
		patients = null;
		
		try {
			registreClient = LocateRegistry.getRegistry(InetAddress.getLocalHost().getHostAddress(), 1099);
		} catch (Exception e) {
			System.err.println("client exception");
			e.printStackTrace();
		}	
	}
	
	public CabinetRemote getPatientsClient() throws RemoteException {
		return patients;
	}
	
	public void setPatientsClient(CabinetRemote cab) throws RemoteException {
		patients = cab;
	}
	
	public Registry getClientRegistre() throws RemoteException {
		return registreClient;
	}
	
	public CabinetRemote getCabinetStub(String url) throws RemoteException {
		
		try {
			patients = (CabinetRemote) registreClient.lookup(url);
			return patients;
			
		} catch (Exception e) {
			return null;
		}	
	}

	public void alerteCabinet(String d) throws RemoteException {
		System.out.println("alerte : " + d);
	}
	
	public void testCodeBase() throws RemoteException {
		Operation dossierR = new Operation();
		patients.getPatientWithNameRemote("Mona").setDossierRemote(dossierR);
	}
	
}
