package remote;
import java.rmi.*;

public interface EspeceRemote extends Remote {
	
	public String getNomEspece() throws RemoteException;
	
	public int getEsperanceVie() throws RemoteException;
	
	public void setNomEspece(String nomEspece) throws RemoteException;
	
	public void setEsperanceVie(int esperanceVie) throws RemoteException;
}
