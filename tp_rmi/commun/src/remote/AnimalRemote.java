package remote;
import java.rmi.*;
// chaque methode doit pouvoir lever cette exception en cas de crash du servuer, de rupture de liaisons etc...

public interface AnimalRemote extends Remote {
	
	public String getNomCompletAnimal() throws RemoteException;
	
	public EspeceRemote getEspece() throws RemoteException;
	
	public String getNomAnimal() throws RemoteException;
	
	public String getNomMaitre() throws RemoteException;
	
	public String getRace() throws RemoteException;
	
	public void setNomAnimal(String nom) throws RemoteException;
	
	public void setNomMaitre(String nom) throws RemoteException;
	
	public void setRace(String race) throws RemoteException;
	
	public void setEspeceRemote(EspeceRemote espece) throws RemoteException;
	
	public DossierSuiviRemote getDossierRemote() throws RemoteException;
	
	public void setDossierRemote(DossierSuiviRemote dossier) throws RemoteException;
	
}
