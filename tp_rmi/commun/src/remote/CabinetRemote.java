package remote;

import java.rmi.*;
import java.util.ArrayList;

public interface CabinetRemote extends Remote {
	
	public AnimalRemote getPatientRemote(AnimalRemote animal) throws RemoteException;
	
	public void setNewPatientRemote(String nomAnimal, String nomMaitre, String nomEspece, int esperanceVie, String soin, String pathologie) throws RemoteException;
	
	public ArrayList<AnimalRemote> getAllPatientRemote() throws RemoteException;
	
	public AnimalRemote getPatientWithNameRemote(String name) throws RemoteException; 
	
	public ArrayList<ClientRemote> getClients() throws RemoteException;
	
	public ClientRemote getClientIndex(int i) throws RemoteException;
	
	public void setAllClients(ArrayList<ClientRemote> clients) throws RemoteException;
	
	public void setNewClient(ClientRemote client) throws RemoteException;
	
	public void removeClient(ClientRemote client) throws RemoteException;
}
