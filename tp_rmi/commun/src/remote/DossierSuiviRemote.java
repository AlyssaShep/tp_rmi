package remote;
import java.rmi.*;

public interface DossierSuiviRemote extends Remote {
	
	public String getSoin() throws RemoteException;
	
	public String getPathologie() throws RemoteException;
	
	public void setSoin(String soin) throws RemoteException;
	
	public void setPathologie(String pathologie) throws RemoteException;
	
}
