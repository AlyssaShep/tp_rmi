package remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

public interface ClientRemote extends Remote{
	public Registry getClientRegistre() throws RemoteException;
	public CabinetRemote getCabinetStub(String url) throws RemoteException;
	public void alerteCabinet(String d) throws RemoteException;
	public void testCodeBase() throws RemoteException;
}
