package remote;
import java.rmi.*;
import java.io.Serializable;

import java.rmi.server.UnicastRemoteObject;

public class DossierSuivi extends UnicastRemoteObject implements DossierSuiviRemote, Serializable {
	
	private static final long serialVersionUID = 1099;

	private String soin;
	private String pathologie;
	
	public DossierSuivi(String typeSoin) throws RemoteException {
		soin = typeSoin;
		pathologie = "Aucune";
	}
	
	public void DossierSuiviComplete(String soin, String pathologie) throws RemoteException {
		this.setSoin(soin);
		this.setPathologie(pathologie);
	}

	public String getSoin() throws RemoteException {
		return soin;
	}
	
	public String getPathologie() throws RemoteException {
		return pathologie;
	}
	
	public void setSoin(String soin) throws RemoteException {
		this.soin = soin;
	}
	
	public void setPathologie(String pathologie) throws RemoteException {
		this.pathologie = pathologie;
	}
}
