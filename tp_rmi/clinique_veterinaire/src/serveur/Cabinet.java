package serveur;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import remote.*;
import java.util.ArrayList;

public class Cabinet extends UnicastRemoteObject implements CabinetRemote {
	
	private static final long serialVersionUID = 1099;
	
	ArrayList<Animaux> patient;
	ArrayList<ClientRemote> clients;
	
	public Cabinet() throws RemoteException {
		this.patient = new ArrayList<Animaux>();
		this.clients = new ArrayList<ClientRemote>();
	}

	public ArrayList<Animaux> getPatient() throws RemoteException {
		return patient;
	}
	
	public Animaux getPatientIndex(int index) throws RemoteException {
		return patient.get(index);
	}
	
	public ArrayList<ClientRemote> getClients() throws RemoteException {
		return clients;
	}
	
	public ClientRemote getClientIndex(int i) throws RemoteException {
		return clients.get(i);
	}
	
	public AnimalRemote getPatientRemote(AnimalRemote animal) throws RemoteException{
		int size = this.patient.size();
		
		for (int i = 0; i < size; i++) {
			if (animal == this.patient.get(i)) {
				return (AnimalRemote) this.patient.get(i);
			}
		}
		
		return null;
	}
	
	public ArrayList<AnimalRemote> getAllPatientRemote() throws RemoteException {
		ArrayList<AnimalRemote> patients = new ArrayList<AnimalRemote>();
		int size = this.patient.size();
		
		for(int i = 0; i < size; i++) {
			patients.add( (AnimalRemote) this.getPatientIndex(i) );
		}
		
		return patients;
	}
	
	public void setAllClients(ArrayList<ClientRemote> clients) throws RemoteException {
		this.clients = clients;
	}
	
	public void setNewClient(ClientRemote client) throws RemoteException {
		this.clients.add(client);
	}

	public void setPatientAll(ArrayList<Animaux> patient) throws RemoteException {
		this.patient = patient;
	}
	
	public void setNewPatient(Animaux patient) throws RemoteException {
		this.patient.add(patient);
		int size = this.patient.size();
		
	}
	
	public void setNewPatientRemote(String nomAnimal, String nomMaitre, String nomEspece, int esperanceVie, String soin, String pathologie) throws RemoteException {
		Animaux patient = new Animaux();
		DossierSuivi dossier = new DossierSuivi("Aucun");
		dossier.DossierSuiviComplete(soin, pathologie);
		Espece espece = new Espece();
		espece.EspeceComplete(nomEspece, esperanceVie);
		patient = patient.AnimauxComplete(nomAnimal, nomMaitre, espece, pathologie, dossier);
		
		int size = this.patient.size();
		
		if (size == 100) {
			System.out.println("Alerte ! Le nombre de patients de 100");
			for(ClientRemote cl : this.clients) {
				cl.alerteCabinet("La nombre de patients est de 100");
			}
		}
		
		if (size == 500) {
			System.out.println("Alerte ! Le nombre de patients de 500");
			for(ClientRemote cl : this.clients) {
				cl.alerteCabinet("La nombre de patients est de 500");
			}
		}
		
		if (size == 1000) {
			System.out.println("Alerte ! Le nombre de patients de 1000");
			for(ClientRemote cl : this.clients) {
				cl.alerteCabinet("La nombre de patients est de 1000");
			}
		}
		
		this.patient.add(patient);
	}
	
	public AnimalRemote getPatientWithNameRemote(String name) throws RemoteException {
		
		int size = patient.size();
		for (int i = 0; i < size; i++) {
			if(patient.get(i).getNomAnimal().equals(name)) {
				return (AnimalRemote) patient.get(i);
			}
		}
		
		return null;
	}
	
	public void removeClient(ClientRemote client) throws RemoteException {
		int size = this.clients.size();
		
		for(int i = 0; i < size; i++) {
			if (client == this.clients.get(i)) {
				this.clients.remove(i);
				break;
			}
		}
	}
	
	public void removePatient(Animaux patient) throws RemoteException {
		int size = this.patient.size();
		
		for (int i = 0; i < size; i++) {
			if (patient == this.patient.get(i)) {
				this.patient.remove(i);
				size -=1;
				
				if (size == 100) {
					System.out.println("affichage alerte");
					for(ClientRemote cl : this.clients) {
						cl.alerteCabinet("La nombre de patients est de 100");
					}
				}
				
				if (size == 500) {
					System.out.println("affichage alerte");
					for(ClientRemote cl : this.clients) {
						cl.alerteCabinet("La nombre de patients est de 500");
					}
				}
				
				if (size == 1000) {
					System.out.println("affichage alerte");
					for(ClientRemote cl : this.clients) {
						cl.alerteCabinet("La nombre de patients est de 1000");
					}
				}
				
				break;
			}
		}
	}
}
