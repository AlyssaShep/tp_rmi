package serveur;

import java.io.File;
import java.net.InetAddress;
import java.rmi.RMISecurityManager;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.TimeUnit;

import remote.DossierSuivi;

import java.util.ArrayList;
import java.nio.file.*;

public class ServeurRMI {

	public static void main(String[] args) {
		
		File file = new File("server.policy");
		String path = file.getPath();
//		System.out.println("path " + path);
		System.setProperty("java.security.policy","file:" + path);
		
		System.setProperty("java.rmi.server.codebase", "file:/codebase/.");
		
		try {
			
//			if (System.getSecurityManager() == null) {
//		        System.setSecurityManager(new RMISecurityManager());
//		      }
			
			// creation des objets
			DossierSuivi dossier = new DossierSuivi("Aucun");
			Animaux obj = new Animaux();
			Espece chatGouttiere = new Espece();
			obj.setDossier(dossier);
			obj.setEspece(chatGouttiere);
			Cabinet cabinet = new Cabinet();
			cabinet.setNewPatient(obj);
			
			Animaux obj2 = new Animaux();
			obj2 = obj2.AnimauxComplete("Amber", "Moi", chatGouttiere, "chat", dossier);
			cabinet.setNewPatient(obj2);
			
			// export
			
			Registry registry = LocateRegistry.createRegistry(1099);
//			Registry registry = LocateRegistry.getRegistry(1099);
			
			if (registry == null) {
				
				System.err.println("RMIRegistry not found");
				
			} else {
				
				System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostAddress());
				String url = "rmi://" + InetAddress.getLocalHost().getHostAddress() + "/cabinet";
				System.out.println("Enregistrement de l objet avec url :" + url);
				registry.rebind(url, cabinet);
				
				while(true) {
					
				}

			}
			
		} catch(Exception e) {
			System.err.println("server exception");
			e.printStackTrace();
		}

	}

}
