package serveur;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import remote.*;

public class Espece extends UnicastRemoteObject implements EspeceRemote {
	
	private static final long serialVersionUID = 1099;
	
	private String nomEspece;
	private int esperanceVie;
	
	public Espece() throws RemoteException {
		nomEspece = "chat de gouttiere";
		esperanceVie = 20;
	}
	
	public void EspeceComplete(String nomEspece, int esperanceVie) throws RemoteException {
		this.setEsperanceVie(esperanceVie);
		this.setNomEspece(nomEspece);
	}

	public String getNomEspece() throws RemoteException {
		return nomEspece;
	}

	public void setNomEspece(String nomEspece) throws RemoteException {
		this.nomEspece = nomEspece;
	}

	public int getEsperanceVie() throws RemoteException {
		return esperanceVie;
	}

	public void setEsperanceVie(int esperanceVie) throws RemoteException {
		this.esperanceVie = esperanceVie;
	}
	
	@Override
	public String toString()  {
		return "Espece [nom=" + nomEspece + ", esperance de vie = " + esperanceVie +"]";
	}
	
}
