package serveur;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;

import remote.AnimalRemote;
import remote.DossierSuivi;
import remote.EspeceRemote;
import remote.DossierSuiviRemote;

public class Animaux extends UnicastRemoteObject implements AnimalRemote {
	
	private static final long serialVersionUID = 1099;
	
	private String nomAnimal;
	private String nomMaitre;
	private Espece espece;
	private String race;
	private DossierSuiviRemote dossier;
 
	public Animaux() throws RemoteException {
		nomAnimal = "Mona";
		nomMaitre = "Maitre";
		espece    = new Espece();
		race      = "ecaille de tortue";
		dossier   = null;
	}
	
	public Animaux AnimauxComplete(String nomAnimal, String nomMaitre, Espece espece, String race, DossierSuiviRemote dossier) throws RemoteException {

		this.setNomAnimal(nomAnimal);
		this.setNomMaitre(nomMaitre);
		this.setEspece(espece);
		this.setRace(race);
		this.setDossierRemote(dossier);
		
		return this;
	}

	public String getNomAnimal() throws RemoteException {
		return nomAnimal;
	}
	
	public void setNomAnimal(String nomAnimal) {
		this.nomAnimal = nomAnimal;
	}
	
	public String getNomMaitre() throws RemoteException {
		return nomMaitre;
	}
	
	public void setNomMaitre(String nomMaitre) {
		this.nomMaitre = nomMaitre;
	}
	
	public EspeceRemote getEspece() throws RemoteException {
		return (EspeceRemote) espece;
	}
	
	public void setEspece(Espece espece) throws RemoteException {
		this.espece = espece;
	}
	
	public void setEspeceRemote(EspeceRemote espece) throws RemoteException {
		this.espece = (Espece) espece;
	}

	public String getRace() throws RemoteException {
		return race;
	}
	
	public void setRace(String race) {
		this.race = race;
	}
	
	public String getNomCompletAnimal() throws RemoteException {
		return nomAnimal + " - " + nomMaitre;
	}
	
	public DossierSuiviRemote getDossier() throws RemoteException {
		return dossier;
	}
	
	public void setDossier(DossierSuivi dossier) {
		this.dossier = dossier;
	}
	
	public void setDossierRemote(DossierSuiviRemote dossier) throws RemoteException {
		this.dossier = dossier;
	}
	
	public DossierSuiviRemote getDossierRemote() throws RemoteException {
		return (DossierSuiviRemote) dossier;
	}
	
	@Override
	public String toString() {
		try {
			
			return "Animaux [nomAnimal=" + nomAnimal + ", nomMaitre=" + nomMaitre + ", espece=" + espece + ", race=" + race
					+ "dossier=" + dossier.getSoin() + " - " + dossier.getPathologie() +"]";
			
		} catch (Exception e) {
			System.err.println("client exception");
			e.printStackTrace();
			return null;
		}
	}
	
}
