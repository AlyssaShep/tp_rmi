# TP_RMI

Réalisé par Océane ONGARO et Lorenzo PUCCIO

## Instructions d'installation du TP

Les instructions suivantes sont également disponibles dans le compte-rendu du TP.

- En prérequis : faites tourner le TP sur une machine Linux. L'exéuction via `make` ne trouve pas les classes à exécuter sur Windows.
- A la première compilation du TP, allez dans le dossier `tp_rmi`. Ouvrez un terminal dans ce dossier et exécutez la commande `make` afin de compiler les classes des projets client, serveur et commun.
- Pour lancer le serveur, exécutez la commande `make serveur`.
- Pour lancer un client, exécutez la commande `make client`. Assurez-vous d'exécuter le serveur avant d'exécuter un client.
